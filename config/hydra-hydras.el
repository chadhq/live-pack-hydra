(defhydra hydra-hydras (:color blue
                         :hint nil)
  "
_p_aredit
_e_xpand Region / Mark
_a_propos^^^^^^^^^^ C-c h
rec_t_angle^^^^^^^^ C-x SPC
_m_ultiple Cursors^ C-c M-m
_w_indmove^^^^^^^^^ WW
_r_efactor^^^^^^^^^ C-c r
_g_it-gutter^^^^^^^ C-c C-g
_P_rojectile^^^^^^^ C-x p
_r_

^ ^            "
  ("e" hydra-mark/body :exit t)
  ("p" hydra-paredit/body :exit t)
  ("a" hydra-apropos/body :exit t)
  ("t" hydra-rectangle/body :exit t)
  ("g" hydra-git-gutter/body :exit t)
  ("r" hydra-cljr/body :exit t)
  ("m" multiple-cursors-hydra/body :exit t)
  ("P" hydra-projectile/body :exit t)
  ("w" hydra-window/body :exit t))

(global-set-key (kbd "C-c M-h") 'hydra-hydras/body)
